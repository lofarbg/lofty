#!/usr/bin/env python3
import numpy as np

import astropy.units as u
from astropy.time import Time

from astropy.coordinates import EarthLocation, SkyCoord, get_icrs_coordinates
from astropy.coordinates import solar_system_ephemeris, get_body, GCRS
from astropy.coordinates import SkyOffsetFrame, CartesianRepresentation

#from lofarimaging import skycoord_to_lmn

def skycoord_to_lmn(pos: SkyCoord, phasecentre: SkyCoord):
    """
    Convert astropy sky coordinates into the l,m,n coordinate system
    relative to a phase centre.

    The l,m,n is a RHS coordinate system with
    * its origin on the sky sphere
    * m,n and the celestial north on the same plane
    * l,m a tangential plane of the sky sphere

    Note that this means that l increases east-wards

    This function was taken from https://github.com/SKA-ScienceDataProcessor/algorithm-reference-library
    """

    # Determine relative sky position
    todc = pos.transform_to(SkyOffsetFrame(origin=phasecentre))
    dc = todc.represent_as(CartesianRepresentation)
    dc /= dc.norm()

    # Do coordinate transformation - astropy's relative coordinates do
    # not quite follow imaging conventions
    return dc.y.value, dc.z.value, dc.x.value - 1


def lmn_sources(timestamps, durations, loc, source_names):
    # Array lengths
    ntime = len(timestamps)
    nsrc = len(source_names)
    
    # Get times
    t = Time(timestamps, format="isot", scale="utc") + 0.5 * np.array(durations) * u.s

    # Get location
    loc = EarthLocation.from_geocentric(*loc * u.m)

    # Get phase centre
    zenith = SkyCoord(az=np.zeros(ntime), alt=90 * np.ones(ntime), unit="deg",
                      obstime=t, location=loc, frame="altaz").transform_to(GCRS)

    source_lmn = np.zeros((nsrc, ntime, 3))
    for i, source_name in enumerate(source_names):
        if source_name.lower() in solar_system_ephemeris.bodies:
            p = get_body(source_name.lower(), t, loc)
        else:
            p = get_icrs_coordinates(source_name.lower()) 
        source_lmn[i] = np.array(skycoord_to_lmn(p, zenith)).T

    return np.moveaxis(source_lmn, 1, 0)
                                    
            
