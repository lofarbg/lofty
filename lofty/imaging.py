#/usr/bin/env python3
import numpy as np

try:
    import cupy as cp

    def sky_imager(visibilities, baselines, freq, l, m):
        l, m = cp.array(l).astype("float32"), cp.array(m).astype("float32")
        visibilities = cp.array(visibilities)
    
        # Select and ravel
        c = l**2 + m**2 < 1
        lt, mt = l[c].ravel(), m[c].ravel()
        nt = np.sqrt(1 - lt**2 - mt**2) 

        u, v, w = cp.array(baselines).astype("float32").T
        prod = (u[:, :, np.newaxis] * lt +
                v[:, :, np.newaxis] * mt +
                w[:, :, np.newaxis] * (nt - 1))
        phase = -2 * cp.pi * freq * prod / 299792458.0
        prod = None
        pr, pi = cp.cos(phase), cp.sin(phase)
        phase = None
        vr, vi = cp.real(visibilities), cp.imag(visibilities)
        img = cp.full(np.prod(l.shape), cp.nan, dtype="float32")
        img[c.ravel()] = cp.mean(vr[:, :, cp.newaxis] * pr - vi[:, :, cp.newaxis] * pi, axis=(0, 1))

        return cp.asnumpy(img.reshape(l.shape))
except:
    def sky_imager(visibilities, baselines, freq, l, m):
        l, m = l.astype("float32"), m.astype("float32")
        
        # Select and ravel
        c = l**2 + m**2 < 1
        lt, mt = l[c].ravel(), m[c].ravel()
        nt = np.sqrt(1 - lt**2 - mt**2) 

        u, v, w = baselines.astype("float32").T
        prod = (u[:, :, np.newaxis] * lt +
                v[:, :, np.newaxis] * mt +
                w[:, :, np.newaxis] * (nt - 1))
        phase = -2 * np.pi * freq * prod / 299792458.0
        pr, pi = np.cos(phase), np.sin(phase)
        vr, vi = np.real(visibilities), np.imag(visibilities)
        img = np.full(np.prod(l.shape), np.nan, dtype="float32")
        img[c.ravel()] = np.mean(vr[:, :, np.newaxis] * pr - vi[:, :, np.newaxis] * pi, axis=(0, 1))
        
        return img.reshape(l.shape)

