#!/usr/bin/env python3
import numpy as np

def get_station_type(station_name: str) -> str:
    """
    Get the station type, one of 'intl', 'core' or 'remote'

    Args:
        station_name: Station name, e.g. "DE603LBA" or just "DE603"

    Returns:
        str: station type, one of 'intl', 'core' or 'remote'

    Example:
        >>> get_station_type("DE603")
        'intl'
    """
    if station_name[0] == "C":
        return "core"
    elif station_name[0] == "R" or station_name[:5] == "PL611":
        return "remote"
    else:
        return "intl"

def get_full_station_name(station_name: str, antenna_set: str) -> str:
    """
    Get full station name with the field appended, e.g. DE603LBA

    Args:
        station_name (str): Short station name, e.g. 'DE603'
        antenna_set (str): antenna_set, e.g. LBA_OUTER

    Returns:
        str: Full station name, e.g. DE603LBA

    Example:
        >>> get_full_station_name("DE603", 'LBA_INNER')
        'DE603LBA'

        >>> get_full_station_name("LV614", 'HBA')
        'LV614HBA'

        >>> get_full_station_name("CS013LBA", 'LBA_OUTER')
        'CS013LBA'

        >>> get_full_station_name("CS002", 'LBA_OUTER')
        'CS002LBA'
    """
    if len(station_name) > 5:
        return station_name
    elif antenna_set[0:3] in ["LBA", "HBA"]:
        station_name += antenna_set[0:3]

    return station_name

def freq_from_sb(sb: int, band: str) -> float:
    """
    Convert central frequency to subband number

    Args:
        sb: subband number
        band: filter band

    Returns:
        float: frequency in Hz

    Example:
        >>> freq_from_sb(297, '30_90')
        58007812.5
    """
    if band not in ["10_90", "30_90", "110_190", "170_230", "210_250"]:
        return None
    
    if band == "10_90" or band == "30_90":
        clock, zone = 200e6, 1
    elif band == "110_190":
        clock, zone = 200e6, 2
    elif band == "170_230":
        clock, zone = 160e6, 3
    elif band == "210_250":
        clock, zone = 200e6, 3

    sb_bandwidth = 0.5 * clock / 512.
    freq_offset = 0.5 * clock * (zone -1)
    freq = (sb * sb_bandwidth) + freq_offset
    return freq

def get_station_xyz(station_name: str, antenna_set: str, db):
    """
    Get XYZ coordinates for the relevant subset of antennas in a station.
    The XYZ system is defined as the PQR system rotated along the R axis to make
    the Q-axis point towards local north.

    Args:
        station_name: Station name, e.g. 'DE603LBA' or 'DE603'
        antenna_set: antenna_set, e.g. LBA_OUTER
        db: instance of LofarAntennaDatabase from lofarantpos

    Returns:
        np.array: Antenna xyz, shape [n_ant, 3]
        np.array: rotation matrix pqr_to_xyz, shape [3, 3]

    Example:
        >>> from lofarantpos.db import LofarAntennaDatabase
        >>> db = LofarAntennaDatabase()
        >>> xyz, _ = get_station_xyz("DE603", "LBA_OUTER", db)
        >>> xyz.shape
        (96, 3)
        >>> f"{xyz[0, 0]:.7f}"
        '2.7033776'

        >>> xyz, _ = get_station_xyz("LV614", "HBA", db)
        >>> xyz.shape
        (96, 3)
    """
    station_pqr = get_station_pqr(station_name, antenna_set, db)

    station_name = get_full_station_name(station_name, antenna_set)

    rotation = db.rotation_from_north(station_name)

    pqr_to_xyz = np.array([[np.cos(-rotation), -np.sin(-rotation), 0],
                           [np.sin(-rotation), np.cos(-rotation), 0],
                           [0, 0, 1]])

    station_xyz = (pqr_to_xyz @ station_pqr.T).T

    return station_xyz, pqr_to_xyz

def get_station_pqr(station_name: str, antenna_set: str, db):
    """
    Get PQR coordinates for the relevant subset of antennas in a station.

    Args:
        station_name: Station name, e.g. 'DE603LBA' or 'DE603'
        antenna_set: antenna_set, e.g. LBA_INNER
        db: instance of LofarAntennaDatabase from lofarantpos

    Example:
        >>> from lofarantpos.db import LofarAntennaDatabase
        >>> db = LofarAntennaDatabase()
        >>> pqr = get_station_pqr("DE603", "LBA_OUTER", db)
        >>> pqr.shape
        (96, 3)
        >>> pqr[0, 0]
        1.7434713

        >>> pqr = get_station_pqr("LV614", "HBA", db)
        >>> pqr.shape
        (96, 3)
    """
    full_station_name = get_full_station_name(station_name, antenna_set)
    station_type = get_station_type(full_station_name)

    all_pqr = db.antenna_pqr(full_station_name)
    if "LBA" in antenna_set:
        if antenna_set == "LBA_OUTER":
            station_pqr = all_pqr[48:, :]
        elif antenna_set == "LBA_INNER":
            station_pqr = all_pqr[:48, :]
        elif antenna_set == "LBA_SPARSE_EVEN":
            station_pqr = np.ravel(np.column_stack((all_pqr[:48:2], all_pqr[49::2]))).reshape(48, 3)
        elif antenna_set == "LBA_SPARSE_ODD":
            station_pqr = np.ravel(np.column_stack((all_pqr[1:48:2], all_pqr[48::2]))).reshape(48, 3)
        else:
            station_pqr = all_pqr
    elif "HBA" in antenna_set:
        selected_dipole_config = {
            'intl': GENERIC_INT_201512, 'remote': GENERIC_REMOTE_201512, 'core': GENERIC_CORE_201512
        }
        selected_dipoles = selected_dipole_config[station_type] + \
            np.arange(len(selected_dipole_config[station_type])) * 16
        single_dipole_pqr = db.hba_dipole_pqr(full_station_name)[selected_dipoles]
        if antenna_set == "HBA_SINGLE":
            station_pqr = single_dipole_pqr
        elif antenna_set == "HBA0_SINGLE":
            station_pqr = single_dipole_pqr[:24, :]
        elif antenna_set == "HBA1_SINGLE":
            station_pqr = single_dipole_pqr[24:, :]
        elif antenna_set == "HBA0":
            station_pqr = all_pqr[:24, :]
        elif antenna_set == "HBA1":
            station_pqr = all_pqr[24:, :]
        else:
            station_pqr = all_pqr
        
    return station_pqr.astype('float32')

