#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter, MultipleLocator
from matplotlib.patches import Circle

def decibel(x):
    return 10 * np.log10(x)


def plot_xst_matrix(metadata, data, isub):
    name = f"{metadata['station_name']}{metadata['antennafield']}"
    t = metadata["timestamps"][isub]
    subband = metadata["subbands"][isub]

    # Extract polarizations
    cube_xx = data[isub, 0::2, 0::2]
    cube_xy = data[isub, 0::2, 1::2]
    cube_yx = data[isub, 1::2, 0::2]
    cube_yy = data[isub, 1::2, 1::2]

    # Add data to single image
    nant = metadata["nant"]
    if nant == 96:
        offset = 12
    else:
        offset = 8
    tick_step = nant // 6
        
    # Get bad antennas
    mask = metadata["antenna_usage_mask"][:nant]
    bad_ant = np.arange(nant)[~mask]
    bad_xx = bad_ant.copy()
    bad_yy = bad_ant.copy()

    # Create plot
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 10))

    fig.suptitle(f"{name}: {t} SB{subband}")

    # XY in lower right, XX in upper left
    xst_img = np.zeros((offset + nant, offset + nant), dtype=cube_xx.dtype)
    xst_img[offset:offset+nant, :nant] += cube_xx
    xst_img[:nant, offset:offset+nant] += cube_xy.T

    # Amplitude
    ax1_top = ax1.secondary_xaxis("top")
    ax1_right = ax1.secondary_yaxis("right")

    img = ax1.imshow(10 * np.log10(np.abs(xst_img)), origin="lower", cmap="magma")
    ticks = np.arange(0, nant + 1, tick_step)
    labels = [f"{x}" for x in ticks]
    ax1_top.set_xticks(ticks)
    ax1_top.set_xticklabels(labels)
    ax1_top.xaxis.set_minor_locator(MultipleLocator(4))    
    ax1_right.set_yticks(ticks)
    ax1_right.set_yticklabels(labels)
    ax1_right.yaxis.set_minor_locator(MultipleLocator(4))
    ax1.set_xticks(offset + bad_xx)
    ax1.set_xticklabels([f"{x}" for x in bad_xx])
    ax1.set_xlabel("XY Antenna number")
    ax1.set_yticks(offset + bad_yy)
    ax1.set_yticklabels([f"{x}" for x in bad_yy])
    ax1.set_ylabel("XX Antenna number")

    fig.colorbar(img, ax=ax1, extend="both", shrink=0.8, location="bottom", label="Power (dB)")

    # Phase
    ax2_top = ax2.secondary_xaxis("top")
    ax2_right = ax2.secondary_yaxis("right")
    
    img = ax2.imshow(np.angle(xst_img) * 180 / np.pi, origin="lower", vmin=-180, vmax=180, cmap="twilight")
    labels = [f"{x}" for x in ticks]
    ax2_top.set_xticks(ticks)
    ax2_top.set_xticklabels(labels)
    ax2_top.xaxis.set_minor_locator(MultipleLocator(4))
    ax2_right.set_yticks(ticks)
    ax2_right.set_yticklabels(labels)
    ax2_right.yaxis.set_minor_locator(MultipleLocator(4))

    ax2.set_xticks(offset + bad_xx)
    ax2.set_xticklabels([f"{x}" for x in bad_xx])
    ax2.set_xlabel("XY Antenna number")
    ax2.set_yticks(offset + bad_yy)
    ax2.set_yticklabels([f"{x}" for x in bad_yy])
    ax2.set_ylabel("XX Antenna number")

    fig.colorbar(img, ax=ax2, extend="both", shrink=0.8, location="bottom", label="Phase (deg)")

    # YY in lower right, YX in upper left
    xst_img = np.zeros((offset + nant, offset + nant), dtype=cube_xx.dtype)
    xst_img[offset:offset+nant, :nant] += cube_yx
    xst_img[:nant, offset:offset+nant] += cube_yy.T

    # Amplitude
    ax3_top = ax3.secondary_xaxis("top")
    ax3_right = ax3.secondary_yaxis("right")

    img = ax3.imshow(10 * np.log10(np.abs(xst_img)), origin="lower", cmap="magma")
    ticks = np.arange(0, nant + 1, tick_step)
    labels = [f"{x}" for x in ticks]
    ax3_top.set_xticks(ticks)
    ax3_top.set_xticklabels(labels)
    ax3_top.xaxis.set_minor_locator(MultipleLocator(4))    
    ax3_right.set_yticks(ticks)
    ax3_right.set_yticklabels(labels)
    ax3_right.yaxis.set_minor_locator(MultipleLocator(4))
    ax3.set_xticks(offset + bad_xx)
    ax3.set_xticklabels([f"{x}" for x in bad_xx])
    ax3.set_xlabel("YY Antenna number")
    ax3.set_yticks(offset + bad_yy)
    ax3.set_yticklabels([f"{x}" for x in bad_yy])
    ax3.set_ylabel("YX Antenna number")

    fig.colorbar(img, ax=ax3, extend="both", shrink=0.8, location="bottom", label="Power (dB)")

    # Phase
    ax4_top = ax4.secondary_xaxis("top")
    ax4_right = ax4.secondary_yaxis("right")
    
    img = ax4.imshow(np.angle(xst_img) * 180 / np.pi, origin="lower", vmin=-180, vmax=180, cmap="twilight")
    labels = [f"{x}" for x in ticks]
    ax4_top.set_xticks(ticks)
    ax4_top.set_xticklabels(labels)
    ax4_top.xaxis.set_minor_locator(MultipleLocator(4))
    ax4_right.set_yticks(ticks)
    ax4_right.set_yticklabels(labels)
    ax4_right.yaxis.set_minor_locator(MultipleLocator(4))

    ax4.set_xticks(offset + bad_xx)
    ax4.set_xticklabels([f"{x}" for x in bad_xx])
    ax4.set_xlabel("YY Antenna number")
    ax4.set_yticks(offset + bad_yy)
    ax4.set_yticklabels([f"{x}" for x in bad_yy])
    ax4.set_ylabel("YX Antenna number")

    fig.colorbar(img, ax=ax4, extend="both", shrink=0.8, location="bottom", label="Phase (deg)")
    
    plt.show()

def plot_sst_timeseries(metadata, data, subband):
    # Timestamp
    t = mdates.date2num(metadata["timestamps"])

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(10, 6))

    date_format = mdates.DateFormatter("%F\n%H:%M:%S")
    fig.autofmt_xdate(rotation=0, ha="center")

    # Select subband
    c = metadata["subbands"] == subband
    px = decibel(data[:, 0, :, c].squeeze())
    py = decibel(data[:, 1, :, c].squeeze())

    mask = metadata["antenna_usage_mask"]
    for iant in range(metadata["nant"]):
        if mask[iant]:
            color = "k"
        else:
            color = "r"
        ax1.plot_date(t, px[:, iant], color, alpha=0.1)
        ax2.plot_date(t, py[:, iant], color, alpha=0.1)
            
    field = metadata["antennafield"]
    if "HBA" in field:
        tile_beam = metadata["tile_beam_pointing_direction"]
    else:
        tile_beam = ""
    ax1.set_title(f"X polarization {field}, SB{subband} {tile_beam}")
    ax1.xaxis.set_major_formatter(date_format)
    ax1.set_ylabel("Power (dB)")
    ax2.set_title(f"Y polarization {field}, SB{subband} {tile_beam}")
    ax2.xaxis.set_major_formatter(date_format)
    ax2.set_xlabel("Date (UTC)")
    ax2.set_ylabel("Power (dB)")
        
    plt.show()

def plot_bst_timeseries(metadata, data, subband):
    # Timestamp
    t = mdates.date2num(metadata["timestamps"])

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(10, 6))

    date_format = mdates.DateFormatter("%F\n%H:%M:%S")
    fig.autofmt_xdate(rotation=0, ha="center")

    # Select subband
    c = metadata["subbands"] == subband
    px = decibel(data[:, 0, c].squeeze())
    py = decibel(data[:, 1, c].squeeze())

    print(px.shape)
    print(np.sum(c))

    ax1.plot_date(t, px, "k", alpha=1)
    ax2.plot_date(t, py, "k", alpha=1)
            
    field = metadata["antennafield"]
    if "HBA" in field:
        tile_beam = metadata["tile_beam_pointing_direction"]
    else:
        tile_beam = ""
    ax1.set_title(f"X polarization {field}, SB{subband} {tile_beam}")
    ax1.xaxis.set_major_formatter(date_format)
    ax1.set_ylabel("Power (dB)")
    ax2.set_title(f"Y polarization {field}, SB{subband} {tile_beam}")
    ax2.xaxis.set_major_formatter(date_format)
    ax2.set_xlabel("Date (UTC)")
    ax2.set_ylabel("Power (dB)")
        
    plt.show()

    
def freq_from_sb(sb, band):
    if "LBA" in band:
        freq = sb * 0.1953125
    elif "HBA_110" in band:
        freq = 100 + sb * 0.1953125
    elif "HBA_210" in band:
        freq = 200 + sb * 0.1953125

    return freq
    
def plot_sst_bandpass(metadata, data):
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 6), sharex=True, gridspec_kw={"height_ratios": [0.2, 0.8], "hspace": 0.02, "wspace": 0.15})

    # Frequencies
    band_x, band_y = metadata["frequency_bands"][0]
    freq_x = np.array([freq_from_sb(sb, band_x) for sb in metadata["subbands"]])
    freq_y = np.array([freq_from_sb(sb, band_y) for sb in metadata["subbands"]])    

    px = decibel(data[0, 0, :, :].squeeze())
    py = decibel(data[0, 1, :, :].squeeze())
    nant = metadata["nant"]
    mask = metadata["antenna_usage_mask"][:nant] # Array is longer by default
    for iant in range(nant):
        if mask[iant]:
            color = "k"
        else:
            color = "r"
        ax1.plot(freq_x, px[iant], color, linewidth=0.2)
        ax2.plot(freq_y, py[iant], color, linewidth=0.2)

    img_ax3 = ax3.imshow(px, aspect="auto", origin="lower", interpolation="None",
                          extent=[np.min(freq_x), np.max(freq_x), 0, nant])
    cbar_ax3 = plt.colorbar(img_ax3, ax=ax3, extend="both", shrink=0.99,
                            location="bottom", label="Power (dB)")
    img_ax4 = ax4.imshow(py, aspect="auto", origin="lower", interpolation="None",
                         extent=[np.min(freq_y), np.max(freq_y), 0, nant])               
    cbar_ax4 = plt.colorbar(img_ax4, ax=ax4, extend="both", shrink=0.99, location="bottom", label="Power (dB)")

    freq_ticks = np.arange(np.min(freq_x), np.max(freq_x), 10)
    freq_labels = [f"{x:g}" for x in freq_ticks]
    
    if nant == 96:
        ticks = np.arange(0, nant + 1, 16)        
    else:
        ticks = np.arange(0, nant + 1, 4)
    labels = [f"{x}" for x in ticks]

    ax1.set_ylabel("Power (dB)")
#    ax1.set_title(title)
    
    ax3.set_xticks(freq_ticks)
    ax3.set_xticklabels(freq_labels)
    ax3.xaxis.set_minor_locator(MultipleLocator(4))  
    ax3.set_yticks(ticks)
    ax3.set_yticklabels(labels)
    ax3.yaxis.set_minor_locator(MultipleLocator(4))  
    
    ax3.set_xlabel("Frequency (MHz)")
    ax3.set_ylabel("Antenna number")

    bad = np.arange(nant)[mask == False]
    
    ax3_right = ax3.secondary_yaxis("right")
    ax3_right.set_yticks(bad + 0.5)
    ax3_right.set_yticklabels([f"{x}" for x in bad])

    ax2.set_ylabel("Power (dB)")
#    ax1.set_title(title)
    
    ax4.set_xticks(freq_ticks)
    ax4.set_xticklabels(freq_labels)
    ax4.xaxis.set_minor_locator(MultipleLocator(4))  
    ax4.set_yticks(ticks)
    ax4.set_yticklabels(labels)
    ax4.yaxis.set_minor_locator(MultipleLocator(4))  
    
    ax4.set_xlabel("Frequency (MHz)")
    ax4.set_ylabel("Antenna number")

    ax4_right = ax4.secondary_yaxis("right")
    ax4_right.set_yticks(bad + 0.5)
    ax4_right.set_yticklabels([f"{x}" for x in bad])
    
    
    plt.show()
    

def plot_sst_dynamic_spectrum(metadata, data, iant, pol):
    # Timestamps
    t = mdates.date2num(metadata["timestamps"])
    
    # Frequencies
    band_x, band_y = metadata["frequency_bands"][0]
    freq_x = np.array([freq_from_sb(sb, band_x) for sb in metadata["subbands"]])
    freq_y = np.array([freq_from_sb(sb, band_y) for sb in metadata["subbands"]])    

    if iant >= 0:
        data = data[:, :, iant, :]
    elif iant == -1:
        data = np.mean(data, axis=2)

    print(data.shape)
    
    if pol.lower() == "x":
        p = decibel(data[:, 0, :].squeeze())
    elif pol.lower() == "y":
        p = decibel(data[:, 1, :].squeeze())
    else:
        p = decibel(np.sum(data[:, :, :], axis=1).squeeze())

    fig, ax = plt.subplots(figsize=(10, 6))

    date_format = mdates.DateFormatter("%F\n%H:%M:%S")
    fig.autofmt_xdate(rotation=0, ha="center")

    vmin, vmax = np.percentile(p, (5, 99))
    ax.imshow(p.T, origin="lower", aspect="auto", interpolation="None", vmin=vmin, vmax=vmax,
               extent=[np.min(t), np.max(t), np.min(freq_x), np.max(freq_x)])

    ax.xaxis_date()
    ax.xaxis.set_major_formatter(date_format)
    ax.set_xlabel("Date (UTC)")
    ax.set_ylabel("Frequency (MHz)")

    plt.show()

def plot_bst_dynamic_spectrum(metadata, data, pol):
    # Timestamps
    t = mdates.date2num(metadata["timestamps"])
    
    # Frequencies
    band_x, band_y = metadata["frequency_bands"][0]
    freq_x = np.array([freq_from_sb(sb, band_x) for sb in metadata["subbands"]])
    freq_y = np.array([freq_from_sb(sb, band_y) for sb in metadata["subbands"]])    

    if pol.lower() == "x":
        p = decibel(data[:, 0, :].squeeze())
    elif pol.lower() == "y":
        p = decibel(data[:, 1, :].squeeze())
    else:
        p = decibel(np.sum(data[:, :, :], axis=1).squeeze())

    fig, ax = plt.subplots(figsize=(10, 6))

    date_format = mdates.DateFormatter("%F\n%H:%M:%S")
    fig.autofmt_xdate(rotation=0, ha="center")

    vmin, vmax = np.percentile(p, (5, 99))
    ax.imshow(p.T, origin="lower", aspect="auto", interpolation="None", vmin=vmin, vmax=vmax,
               extent=[np.min(t), np.max(t), np.min(freq_x), np.max(freq_x)])

    ax.xaxis_date()
    ax.xaxis.set_major_formatter(date_format)
    ax.set_xlabel("Date (UTC)")
    ax.set_ylabel("Frequency (MHz)")

    plt.show()
    
def sky_plot(img, title, subtitle, source_names, source_lmn, fig):
    if fig is None:
        fig = plt.figure(figsize=(10, 10))

    ax = fig.add_subplot(1, 1, 1)
    circle1 = Circle((0, 0), 1.0, edgecolor='k', fill=False, facecolor='none', alpha=0.3)

    # Plot image and colorbar
    cimg = ax.imshow(img, origin="lower", cmap="Blues_r", extent=[1, -1, -1, 1])
    cbar = fig.colorbar(cimg, ax=ax, location="right", format="%.2e")
    ax.set_axis_off()
    ax.add_artist(circle1)

    # Plot sources
    for name, lmn in zip(source_names, source_lmn):
        if lmn[2] >= -1:
            ax.plot(lmn[0], lmn[1], marker="x", color="k")
            ax.text(lmn[0], lmn[1], f" {name}")

    # Plot the compass directions
    for direction, xoffset, yoffset in zip(["E", "W", "N", "S"], [1.05, -1.05, 0.0, 0.0], [0.0, 0.0, 1.05, -1.05]):
        ax.text(xoffset, yoffset, direction, horizontalalignment='center', verticalalignment='center', color='k')

    ax.text(0.5, 1.1, title, fontsize=17, ha="center", va="bottom", transform=ax.transAxes)
    ax.text(0.5, 1.05, subtitle, fontsize=12, ha="center", va="bottom", transform=ax.transAxes)

    return fig
    
