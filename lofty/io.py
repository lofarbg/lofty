#!/usr/bin/env python3
import h5py

import numpy as np

from datetime import datetime

def read_single_xst(xst_filename, fill_empty=False):
    # Open HDF5 file
    h5 = h5py.File(xst_filename, "r")

    # Store main header meta data
    metadata = {}
    for key in ["antenna_names", "antenna_quality", "antenna_type", "antenna_usage_mask", "station_name"]:
        metadata[key] = h5["/"].attrs[key]

    # Antennafield
    metadata["antennafield"] = h5["/"].attrs["antennafield_device"].replace("STAT/AFH/", "").replace("STAT/AFL/", "")
    
    # Number of subintegrations
    nsub = len(h5.keys())
    metadata["nsub"] = nsub
        
    # Loop over keys
    data_dict = []
    for key in h5.keys():
        # Keys in group
        attribute_keys = list(h5[key].attrs.keys())

        # Keys to store in dict
        keys_to_store = ["frequency_band", "subbands", "timestamp", "integration_interval"]
        
        # Create dict
        d = {"data": np.array(h5[key])}
        for key_to_store in keys_to_store:
            if key_to_store in attribute_keys:
                d[key_to_store] = h5[key].attrs[key_to_store]
            else:
                print(f"Failed to find {key_to_store} in {key}")
                d[key_to_store] = None

        # Store tile beam for HBA
        if "HBA" in metadata["antennafield"]:
            d["tile_beam_pointing_direction"] = h5[key].attrs["tile_beam_pointing_direction"]
                
        # Store dict
        data_dict.append(d)

    # Store arrays
    metadata["timestamps"] = np.array([d["timestamp"] for d in data_dict])
    metadata["durations"] = np.array([d["integration_interval"] for d in data_dict])
    metadata["subbands"] = [data_dict[i]["subbands"][0] for i in range(nsub)]

    # Assume first entry holds for all (ASSUMPTION!!)
    metadata["frequency_bands"] = data_dict[0]["frequency_band"]
    if "HBA" in metadata["antennafield"]:
        metadata["tile_beam_pointing_direction"] = data_dict[0]["tile_beam_pointing_direction"][0]
    else:
        metadata["tile_beam_pointing_direction"] = ""

    # Data shape
    nrcu, _ = data_dict[0]["data"].shape
    nant = nrcu // 2
    metadata["nrcu"] = nrcu
    metadata["nant"] = nant
    
    # Store as nsub x nrcu x nrcu
    data = np.zeros((nsub, nrcu, nrcu), dtype=data_dict[0]["data"].dtype)
    for isub in range(nsub):
        data[isub] = data_dict[isub]["data"]

    # Fill empty parts
    if fill_empty:
        data += np.conj(np.transpose(data, (0, 2, 1))) * (data==0)
        
    # Close file
    h5.close()
        
    return metadata, data

def read_xst(xst_filenames, fill_empty=False):
    first = True
    for xst_filename in xst_filenames:
        # Get data
        if first:
            metadata, data = read_single_xst(xst_filename, fill_empty)
            first = False
        else:
            newmetadata, newdata = read_single_xst(xst_filename, fill_empty)

            # Concatenate timestamps
            metadata["timestamps"] = np.hstack((metadata["timestamps"],
                                                newmetadata["timestamps"]))

            # Concatenate integration_interval
            metadata["durations"] = np.hstack((metadata["durations"],
                                                newmetadata["durations"]))

            # Concatenate subbands
            metadata["subbands"] = np.hstack((metadata["subbands"],
                                                newmetadata["subbands"]))
            
            # Concatenate data
            data = np.vstack((data, newdata))

    # Update nsub
    metadata["nsub"] = data.shape[0]
            
    return metadata, data


def read_single_sst(sst_filename):
    # Open HDF5 file
    h5 = h5py.File(sst_filename, "r")

    # Store main header meta data
    metadata = {}
    for key in ["antenna_names", "antenna_quality", "antenna_type", "antenna_usage_mask", "station_name"]:
        metadata[key] = h5["/"].attrs[key]

    # Antennafield
    metadata["antennafield"] = h5["/"].attrs["antennafield_device"].replace("STAT/AFH/", "").replace("STAT/AFL/", "")
    
    # Number of subintegrations
    nsub = len(h5.keys())
    metadata["nsub"] = nsub
        
    # Loop over keys
    data_dict = []
    for key in h5.keys():
        # Keys in group
        attribute_keys = list(h5[key].attrs.keys())

        # Keys to store in dict
        keys_to_store = ["frequency_band", "subbands", "timestamp", "integration_interval"]
        
        # Create dict
        d = {"data": np.array(h5[key])}
        for key_to_store in keys_to_store:
            if key_to_store in attribute_keys:
                d[key_to_store] = h5[key].attrs[key_to_store]
            else:
                print(f"Failed to find {key_to_store} in {key}")
                d[key_to_store] = None

        # Store tile beam for HBA
        if "HBA" in metadata["antennafield"]:
            d["tile_beam_pointing_direction"] = h5[key].attrs["tile_beam_pointing_direction"]
                
        # Store dict
        data_dict.append(d)

    # Store arrays
    metadata["timestamps"] = np.array([d["timestamp"] for d in data_dict])
    metadata["durations"] = np.array([d["integration_interval"] for d in data_dict])
    
    # Assume first entry holds for all (ASSUMPTION!!)
    metadata["subbands"] = data_dict[0]["subbands"]
    metadata["frequency_bands"] = data_dict[0]["frequency_band"]
    if "HBA" in metadata["antennafield"]:
        metadata["tile_beam_pointing_direction"] = data_dict[0]["tile_beam_pointing_direction"][0]
    else:
        metadata["tile_beam_pointing_direction"] = ""
    
    # Data shape
    nrcu, nchan = data_dict[0]["data"].shape
    nant = nrcu // 2
    metadata["nrcu"] = nrcu
    metadata["nant"] = nant
    metadata["nchan"] = nchan
    
    # Store as nsub x npol x nant x nchan
    data = np.zeros((nsub, 2, nant, nchan))
    for isub in range(nsub):
        data[isub, 0] = data_dict[isub]["data"][0::2, :]
        data[isub, 1] = data_dict[isub]["data"][1::2, :]
        
    # Close file
    h5.close()

    return metadata, data

def read_sst(sst_filenames):
    first = True
    for sst_filename in sst_filenames:
        # Get data
        if first:
            metadata, data = read_single_sst(sst_filename)
            first = False
        else:
            newmetadata, newdata = read_single_sst(sst_filename)

            # Concatenate timestamps
            metadata["timestamps"] = np.hstack((metadata["timestamps"],
                                                newmetadata["timestamps"]))

            # Concatenate integration_interval
            metadata["durations"] = np.hstack((metadata["durations"],
                                                newmetadata["durations"]))

            # Concatenate data
            data = np.vstack((data, newdata))

    return metadata, data

def read_single_bst(bst_filename):
    # Open HDF5 file
    h5 = h5py.File(bst_filename, "r")

    # Store main header meta data
    metadata = {}
    for key in ["antenna_names", "antenna_quality", "antenna_type", "antenna_usage_mask", "station_name"]:
        metadata[key] = h5["/"].attrs[key]

    # Antennafield
    metadata["antennafield"] = h5["/"].attrs["antennafield_device"].replace("STAT/AFH/", "").replace("STAT/AFL/", "")
    
    # Number of subintegrations
    nsub = len(h5.keys())
    metadata["nsub"] = nsub
        
    # Loop over keys
    data_dict = []
    for key in h5.keys():
        # Keys in group
        attribute_keys = list(h5[key].attrs.keys())

        # Keys to store in dict
        keys_to_store = ["frequency_band", "subbands", "timestamp", "integration_interval"]
        
        # Create dict
        d = {"data": np.array(h5[key])}
        for key_to_store in keys_to_store:
            if key_to_store in attribute_keys:
                d[key_to_store] = h5[key].attrs[key_to_store]
            else:
                print(f"Failed to find {key_to_store} in {key}")
                d[key_to_store] = None

        # Store tile beam for HBA
        if "HBA" in metadata["antennafield"]:
            d["tile_beam_pointing_direction"] = h5[key].attrs["tile_beam_pointing_direction"]
                
        # Store dict
        data_dict.append(d)

    # Store arrays
    metadata["timestamps"] = np.array([d["timestamp"] for d in data_dict])
    metadata["durations"] = np.array([d["integration_interval"] for d in data_dict])
    
    # Assume first entry holds for all (ASSUMPTION!!)
    metadata["subbands"] = data_dict[0]["subbands"]
    metadata["frequency_bands"] = data_dict[0]["frequency_band"]
    if "HBA" in metadata["antennafield"]:
        metadata["tile_beam_pointing_direction"] = data_dict[0]["tile_beam_pointing_direction"][0]
    else:
        metadata["tile_beam_pointing_direction"] = ""
    
    # Data shape
    nchan, npol = data_dict[0]["data"].shape
    metadata["nchan"] = nchan
    
    # Store as nsub x npol x nchan
    data = np.zeros((nsub, 2, nchan))
    for isub in range(nsub):
        data[isub, 0] = data_dict[isub]["data"][:, 0::2].T
        data[isub, 1] = data_dict[isub]["data"][:, 1::2].T

    # Close file
    h5.close()

    return metadata, data
        
def read_bst(bst_filenames):
    first = True
    for bst_filename in bst_filenames:
        # Get data
        if first:
            metadata, data = read_single_bst(bst_filename)
            first = False
        else:
            newmetadata, newdata = read_single_bst(bst_filename)

            # Concatenate timestamps
            metadata["timestamps"] = np.hstack((metadata["timestamps"],
                                                newmetadata["timestamps"]))

            # Concatenate integration_interval
            metadata["durations"] = np.hstack((metadata["durations"],
                                                newmetadata["durations"]))

            # Concatenate data
            data = np.vstack((data, newdata))

    return metadata, data

def read_acm_cube(xst_filename, fill=True):
    # Open file
    h5 = h5py.File(xst_filename, "r")

    writer_version = h5.attrs["writer_version"]

    # Number of sub integrations
    nsub = len(h5.keys())

    # Loop over sub integrations
    timestrings = []
    for isub, key in enumerate(h5.keys()):
        # Read data
        data = np.array(h5[key])

        # Create cube
        if isub == 0:
            nant = data.shape[0]
            cube = np.zeros((nsub, nant, nant), dtype=data.dtype)
            subbands = np.zeros(nsub, dtype="int64")
            durations = np.zeros(nsub, dtype="float32")

        # Store
        cube[isub] = data

        # Read attributes
        timestrings.append(h5[key].attrs["timestamp"])
        subbands[isub] = h5[key].attrs["data_id_subband_index"]
        durations[isub] = h5[key].attrs["integration_interval"]

    # Decode timestamps
    if writer_version == "0.3":
        timestamps = [datetime.strptime(timestring, "%Y-%m-%dT%H:%M:%S.%f%z") for timestring in timestrings]
    else:
        timestamps = [datetime.strptime(timestring, "%Y-%m-%dT%H:%M:%S.%f") for timestring in timestrings]
        
    #if np.all(np.diagonal(cube[0, 48:96, 48:96]) == 0 + 0j):
    #    # Move block to right location
    #    cube[:, 48:96, 48:96] = cube[:, 48:96, 0:48]
    #    # Zero original block
    #    cube[:, 48:96, 0:48] = 0 + 0j
    
    # Redimension HBA data to 96x96
    if np.all(np.diagonal(cube[0, 96:, 96:]) == 0 + 0j):
        cube = cube[:, :96, :96]

    # Fill empty parts
    if fill:
        cube += np.conj(np.transpose(cube, (0, 2, 1))) * (cube==0)

    # Close file
    h5.close()

    return cube, timestamps, subbands, durations
