#!/usr/bin/env python3
import os
import glob
import argparse

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter, MultipleLocator

from lofty.io import read_sst
from lofty.plotting import plot_sst_timeseries, plot_sst_bandpass, plot_sst_dynamic_spectrum

if __name__ == "__main__":
    # Read command line arguments
    parser = argparse.ArgumentParser(description="Plot SST data")
    parser.add_argument("-d", "--dynspec", help="Plot dynamic spectrum", action="store_true")
    parser.add_argument("-t", "--timeseries", help="Plot timeseries", action="store_true")
    parser.add_argument("-b", "--bandpass", help="Plot bandpass", action="store_true")    
    parser.add_argument("-s", "--subband", help="Subband to plot timeseries [int, default: 300]", default=300, type=int)
    parser.add_argument("-p", "--pol", help="Polarization to plot dynamic spectrum [stokes/X/Y, default: stokes]", default="stokes")
    parser.add_argument("-a", "--ant", help="Antenna to plot dynamic spectrum [int (-1 for mean), default: -1]", default=-1, type=int)
    parser.add_argument("filenames", help="SST filenames", nargs="*", metavar="FILE")
    args = parser.parse_args()

    print(args.filenames)
    print(args.pol)

    # Read data
    metadata, data = read_sst(args.filenames)

    plot_sst_dynamic_spectrum(metadata, data, args.ant, args.pol)
    
    plot_sst_bandpass(metadata, data)
    
    plot_sst_timeseries(metadata, data, args.subband)
