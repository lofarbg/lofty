#!/usr/bin/env python3
import os
import glob
import argparse

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter, MultipleLocator

from lofty.io import read_bst
from lofty.plotting import plot_bst_timeseries, plot_bst_dynamic_spectrum

if __name__ == "__main__":
    # Read command line arguments
    parser = argparse.ArgumentParser(description="Plot BST data")
    parser.add_argument("-d", "--dynspec", help="Plot dynamic spectrum", action="store_true")
    parser.add_argument("-t", "--timeseries", help="Plot timeseries", action="store_true")
    parser.add_argument("-s", "--subband", help="Subband to plot timeseries [int, default: 300]", default=300, type=int)
    parser.add_argument("-p", "--pol", help="Polarization to plot dynamic spectrum [stokes/X/Y, default: stokes]", default="stokes")
    parser.add_argument("filenames", help="SST filenames", nargs="*", metavar="FILE")
    args = parser.parse_args()

    # Read data
    metadata, data = read_bst(args.filenames)

    plot_bst_dynamic_spectrum(metadata, data, args.pol)
    
    plot_bst_timeseries(metadata, data, args.subband)
