#!/usr/bin/env python3
import os
import glob
import tqdm
import argparse

import numpy as np
import matplotlib.pyplot as plt

from lofarantpos.db import LofarAntennaDatabase

from lofty.io import read_xst
from lofty.imaging import sky_imager
from lofty.sources import lmn_sources
from lofty.plotting import sky_plot
from lofty.utils import get_station_type, get_full_station_name, freq_from_sb, get_station_xyz

if __name__ == "__main__":
    # Read command line arguments
    parser = argparse.ArgumentParser(description="Create XST images")
    parser.add_argument("filenames", help="XST filenames", nargs="*", metavar="FILE")
    args = parser.parse_args()

    # Read data
    metadata, data = read_xst(sorted(args.filenames), fill_empty=True)

    # Get information
    station_type = get_station_type(metadata["station_name"])
    antenna_set = metadata["antennafield"]
    station_name = get_full_station_name(metadata["station_name"], antenna_set)

    # Setup the antenna database
    db = LofarAntennaDatabase()

    # Antenna positions
    station_xyz, pqr_to_xyz = get_station_xyz(station_name, antenna_set, db)

    # Get antenna mask
    nant = metadata["nant"]
    mask = metadata["antenna_usage_mask"][:nant]

    # Apply mask to antennas and visibilities
    station_xyz = np.compress(mask, station_xyz, axis=0)
    data = np.compress(np.repeat(mask, 2), data, axis=1)
    data = np.compress(np.repeat(mask, 2), data, axis=2)    
    
    # Generate visibilities
    cube_xx = data[:, 0::2, 0::2].astype("complex64")
    cube_xy = data[:, 0::2, 1::2].astype("complex64")
    cube_yx = data[:, 1::2, 0::2].astype("complex64")
    cube_yy = data[:, 1::2, 1::2].astype("complex64")
    
    # Baselines
    baselines = station_xyz[:, np.newaxis, :] - station_xyz[np.newaxis, :, :]

    # Get frequencies
    nsub = metadata["nsub"]
    freqs = np.zeros(nsub)
    band = metadata["frequency_bands"][0][0].replace("LBA_", "").replace("HBA_", "")
    for isub in range(nsub):
        subband = metadata["subbands"][isub]
        freqs[isub] = freq_from_sb(subband, band)

    # Create imager
    npix_l, npix_m = 205, 205
    l, m = np.meshgrid(np.linspace(-1, 1, npix_l), np.linspace(1, -1, npix_m))

    # Compute visibilities
    vis_i = cube_xx + cube_yy
#    vis_q = cube_xx - cube_yy
#    vis_u = cube_xy + cube_yx
#    vis_v = 1j * (cube_xy - cube_yy)

    source_names = ["Cas A", "Cyg A", "Sun", "Tau A", "Moon", "Jupiter"]
    lmn = lmn_sources(metadata["timestamps"], metadata["durations"], db.phase_centres[station_name], source_names)
    
    # Compute images
    img_i = np.zeros((nsub, npix_l, npix_m))
    for isub in tqdm.tqdm(range(nsub)):
        img_i[isub] = sky_imager(vis_i[isub], baselines, freqs[isub], l, m)

    for isub in range(nsub):
        subband = metadata['subbands'][isub]
        tstr = metadata['timestamps'][isub][:19]
        freq = freqs[isub] * 1e-6
        fname = f"{station_name}_{tstr}_SB{subband:03d}.png".replace(":", "_")

        fig = plt.figure(figsize=(10, 8), dpi=100)
        title = f"All-sky image for {station_name}"
        subtitle = f"SB{subband:03d}, {freq:.3f} MHz, {tstr}"
        sky_plot(img_i[isub], title, subtitle, source_names, lmn[isub], fig)
        plt.savefig(fname)
        plt.close()
        
#    for isub in range(nsub):
#        fig, ax = plt.subplots(figsize=(10, 10))
#        ax.set_title(f"{metadata['timestamps'][isub]} {freqs[isub]}")
#        ax.imshow(img_i[isub], origin="lower")
#        plt.show()
    

    # Write to FITS
    #hdr = fits.Header()

    #hdu = fits.PrimaryHDU(data=img_i, header=hdr)
    #hdu.writeto("stokes_i.fits", overwrite=True)
    #hdu = fits.PrimaryHDU(data=img_q, header=hdr)
    #hdu.writeto("stokes_q.fits", overwrite=True)
    #hdu = fits.PrimaryHDU(data=img_u, header=hdr)
    #hdu.writeto("stokes_u.fits", overwrite=True)
    #hdu = fits.PrimaryHDU(data=img_v, header=hdr)
    #hdu.writeto("stokes_v.fits", overwrite=True)
